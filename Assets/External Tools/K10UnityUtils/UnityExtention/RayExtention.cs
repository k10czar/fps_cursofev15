using UnityEngine;


public static class RayExtention 
{
	public static Vector3 GetWithY( this Ray ray, float y ) 
	{
		var distance = ( ( y - ray.origin.y ) /  ray.direction.y );
		return ray.origin + ( ray.direction * distance );
	}
}