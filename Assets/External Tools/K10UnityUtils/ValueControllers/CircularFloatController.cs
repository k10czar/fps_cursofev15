using UnityEngine;
using System.Collections;

[System.Serializable]
public class CircularFloatController
{
	float m_minimum = 0;
	float m_maximum = 1;

	FloatController m_controller;

	public CircularFloatController( float min, float max, float initial )
	{
		m_minimum = min;
		m_maximum = max;
		m_controller = new FloatController( min - Delta / 2, max + Delta / 2, initial );
	}

	float CircularClamp( float val, float min, float max )
	{
		float d = max - min;
		return ( ( ( ( val - min ) % d ) + d ) % d ) + min;
		//val = ( val - min ) % delta;
		//return ( ( val < 0 ) ? val + delta : val ) + min;
	}

	float Delta { get { return m_maximum - m_minimum; } }

	public float DesiredValue { get { return CircularClamp( m_controller.DesiredValue, m_minimum, m_maximum ); } }
	public float ForceValue { set { m_controller.ForceValue = CircularClamp( value, m_minimum, m_maximum ); } }
	public float Value
	{
		get { return CircularClamp( m_controller.Value, m_minimum, m_maximum ); }
		set
		{
			float newBase = CircularClamp( m_controller.Value, m_minimum, m_maximum );
			m_controller.ForceValue = newBase;
			float val = CircularClamp( value, m_minimum, m_maximum );
			if( Mathf.Abs( val - newBase ) > Delta / 2 )
			{
				if( newBase < m_minimum + ( Delta / 2 ) ) val = m_minimum - Delta + val % Delta;
				else  val = m_maximum + val % Delta;
			}
			m_controller.Value = val;
		}
	}


	public void Update( float deltaTime )
	{
		m_controller.Update( deltaTime );
	}
}
