﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class FloatAnimator
{
    [SerializeField] float m_min = float.MinValue;
    [SerializeField] float m_max = float.MaxValue;
    [SerializeField] float m_acceleration = 60;
    [SerializeField] float m_deacceleration = 60;
    [SerializeField] float m_maxVelocity = 10;
    SubscribersCollection<System.Action> _reachValue = new SubscribersCollection<System.Action>();

    float m_velocity;
    float m_current;
    float m_desired;

    public ISubscribableEvent<System.Action> ReachValue { get { return _reachValue; } }
	public float Value { get { return m_current; } }
	public float CurrentVelocity { get { return m_velocity; } }
	public float MaxVelocity { get { return m_maxVelocity; } }
    public float Max { get { return m_max; } }
    public float Min { get { return m_min; } }

    public void Start( float startValue )
    {
        m_current = Mathf.Clamp( startValue, m_min, m_max );
        m_desired = m_current;
    }

    public void SetMinimum( float min ) { m_min = min; m_desired = Mathf.Clamp( m_desired, m_min, m_max ); m_current = Mathf.Clamp( m_current, m_min, m_max ); }
    public void SetMaximum( float max ) { m_max = max; m_desired = Mathf.Clamp( m_desired, m_min, m_max ); m_current = Mathf.Clamp( m_current, m_min, m_max ); }

    public void SetDesire( float desired )
    {
        m_desired = Mathf.Clamp( desired, m_min, m_max );
    }

    public void Update( float deltaTime )
    {
        float diff = m_desired - m_current;
        var aVel = m_velocity + Mathf.Sign( diff ) * m_acceleration * deltaTime;
        var dVel = Mathf.Sqrt( Mathf.Abs( 2 * m_deacceleration * diff ) );
        var vel = Mathf.Sign( aVel ) * Mathf.Min( Mathf.Abs( aVel ), Mathf.Abs( dVel ) );
        m_velocity = Mathf.Clamp( vel, -m_maxVelocity, m_maxVelocity );

        if( Mathf.Abs( m_velocity * deltaTime ) >= Mathf.Abs( diff ) )
        {
            m_current = m_desired;

            if( m_velocity != 0 )
            {
                foreach( var s in _reachValue.Subscribers ) s();
                m_velocity = 0;
            }
        }
        else
        {
            m_current += m_velocity * deltaTime;

            if( m_current == m_desired )
            {
                foreach( var s in _reachValue.Subscribers ) s();
                m_velocity = 0;
            }
        }
    }
}