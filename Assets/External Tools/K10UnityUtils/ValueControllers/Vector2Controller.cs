using UnityEngine;
using System.Collections;

[System.Serializable]public class Vector2Controller
{
	[SerializeField] FloatController m_componentX = new FloatController();
	[SerializeField] FloatController m_componentY = new FloatController();

	public FloatController X { get { return m_componentX; } }
	public FloatController Y { get { return m_componentY; } }

	public Vector2Controller() { }
	public Vector2Controller( float min, float max ) : base() { m_componentX = new FloatController( min, max, 0 ); m_componentY = new FloatController( min, max, 0 ); }

	public void Update( float delta )
	{
		m_componentX.Update( delta );
		m_componentY.Update( delta );
	}
}
