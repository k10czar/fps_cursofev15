﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class RandomFloatRange
{
    [SerializeField] float _min = 0;
    [SerializeField] float _max = 1;

    public float Random { get { return UnityEngine.Random.Range( _min, _max ); } }

    public RandomFloatRange( float min, float max ) { _min = min; _max = max; }
    public RandomFloatRange() : this( 0, 1 ) { }
}
