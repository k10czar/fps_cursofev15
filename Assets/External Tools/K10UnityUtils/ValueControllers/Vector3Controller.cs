using UnityEngine;
using System.Collections;

[System.Serializable]public class Vector3Controller
{
	public Vector3 Value { get { return new Vector3( X.Value, Y.Value, Z.Value ); } }

	[SerializeField] FloatController m_componentX = new FloatController();
	[SerializeField] FloatController m_componentY = new FloatController();
	[SerializeField] FloatController m_componentZ = new FloatController();

	public FloatController X { get { return m_componentX; } }
	public FloatController Y { get { return m_componentY; } }
	public FloatController Z { get { return m_componentZ; } }

	public Vector3Controller() { }
	public Vector3Controller( Vector3 min, Vector3 max ) : base()
	{
		m_componentX = new FloatController( min.x, max.x, min.x + ( max.x - min.x ) / 2 );
		m_componentY = new FloatController( min.y, max.y, min.y + ( max.y - min.y ) / 2 );
		m_componentZ = new FloatController( min.z, max.z, min.z + ( max.z - min.z ) / 2 );
	}

	public void Update( float delta )
	{
		m_componentX.Update( delta );
		m_componentY.Update( delta );
		m_componentZ.Update( delta );
	}
}
