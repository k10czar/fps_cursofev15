using UnityEngine;
using System.Collections;


[System.Serializable]public class FloatController
{
	[SerializeField] float m_minimum = 0;
	[SerializeField] float m_maximum = 1;
	float m_value = 0;
	float m_desiredValue = 0;

	public FloatController() { }
	public FloatController( float min, float max, float initial ) : this() { m_minimum = min; m_maximum = max; m_value = m_desiredValue = initial; }

	public float Max { get { return m_maximum; } }
	public float Min { get { return m_minimum; } }
	public float Delta { get { return m_maximum - m_minimum; } }
	public float Percent { get { return ( Value - m_minimum ) / Delta; } set { Value = value * Delta + m_minimum; } }

	public float ForcePercent { set { ForceValue = value * Delta + m_minimum; } }
	public float ForceValue { set { m_value = m_desiredValue = Mathf.Clamp( value, m_minimum, m_maximum ); } }
	
	public float DesiredValue { get { return m_desiredValue; } }

	public float Value
	{
		get { return m_value; }
		set
		{
			float val = Mathf.Clamp( value, m_minimum, m_maximum );
			if( m_desiredValue != val )
			{
				m_desiredValue = val;
			}
		}
	}

	public void Update( float delta )
	{
		float newDelta = delta * 5;
		if( newDelta < 1 )
			m_value += ( m_desiredValue - m_value ) * newDelta;
		else
			m_value = m_desiredValue;
	}
}
