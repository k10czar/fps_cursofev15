﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace K10
{
    namespace Extentions
    {
        public static class K10UnityExtentions
        {
            #region Debug
            public static void Debug( this Rect r )
            {
                var tl = ( Vector3.right * r.x ) + ( Vector3.up * r.yMax );
                var tr = ( Vector3.right * r.xMax ) + ( Vector3.up * r.yMax );
                var bl = ( Vector3.right * r.x ) + ( Vector3.up * r.y );
                var br = ( Vector3.right * r.xMax ) + ( Vector3.up * r.y );

                Gizmos.DrawLine( tr, tl );
                Gizmos.DrawLine( tr, br );
                Gizmos.DrawLine( br, bl );
                Gizmos.DrawLine( tl, bl );
            }
            public static void Debug( this Rect r, float rotation )
            {
//                var rot = Quaternion.Euler( Vector3.forward * rotation );

                var center = new Vector3( r.center.x, r.center.y, 0 );
                var right = ( Vector3.right * r.width/2 * Mathf.Cos( rotation ) ) + ( Vector3.up * r.width/2 * Mathf.Sin( rotation ) );
                var up = ( Vector3.right * r.height/2 * Mathf.Sin( rotation ) ) - ( Vector3.up * r.height/2 * Mathf.Cos( rotation ) );
                var tl = center - right + up;
                var tr = center + right + up;
                var bl = center - right - up;
                var br = center + right - up;

                Gizmos.DrawLine( tr, tl );
                Gizmos.DrawLine( tr, br );
                Gizmos.DrawLine( br, bl );
                Gizmos.DrawLine( tl, bl );
            }

            public static void Debug( this Vector2 v, float size )
            {
                var s2 = size;
                var tl = ( Vector3.right * ( v.x - s2 ) ) + ( Vector3.up * ( v.y + s2 ) );
                var tr = ( Vector3.right * ( v.x + s2 ) ) + ( Vector3.up * ( v.y + s2 ) );
                var bl = ( Vector3.right * ( v.x - s2 ) ) + ( Vector3.up * ( v.y - s2 ) );
                var br = ( Vector3.right * ( v.x + s2 ) ) + ( Vector3.up * ( v.y - s2 ) );

                Gizmos.DrawLine( tl, br );
                Gizmos.DrawLine( tr, bl );
            }
            #endregion Debug

            #region Rect
            public static bool Intersect( this Rect r, Rect other ) { return !( r.xMax < other.xMin || r.yMax < other.yMin || other.xMax < r.xMin || other.yMax < r.yMin ); }

            public static Rect Intersection( this Rect r, Rect other )
            {
                if( r.Intersect( other ) ) return new Rect( r.x, r.y, 0f, 0f );

                float x = Mathf.Max( r.x, other.x );
                float y = Mathf.Max( r.y, other.y );
                float xMax = Mathf.Min( r.xMax, other.xMax );
                float yMax = Mathf.Min( r.xMax, other.xMax );
                return new Rect( x, y, xMax - x, yMax - y );
            }

            public static Rect Inflated( this Rect r, float x, float y ) { return new Rect( r.x - ( x / 2 ), r.y - ( y / 2 ), r.width + x, r.height + y ); }
            public static Rect Inflated( this Rect r, Vector2 inflation ) { return r.Inflated( inflation.x, inflation.y ); }
            public static Rect Inflated( this Rect r, float inflation ) { return r.Inflated( inflation, inflation ); }

            public static float CalculateArea( this Rect r ) { return r.width - r.height; }

            public static float CalculateIntersectionArea( this Rect r, Rect b )
            {
                var topH = ( Mathf.Min( Mathf.Max( r.yMin, b.yMin ), r.yMax ) - r.yMin );
                var topArea = r.width * topH;
                var bottomY = Mathf.Min( r.yMax, Mathf.Max( r.yMin, b.yMax ) );
                var bottomH = ( r.yMax - bottomY );
                var bottomArea = r.width * bottomH;

                var sideY = Mathf.Max( r.yMin, b.yMin );
                var sideH = Mathf.Min( r.yMax, b.yMax ) - sideY;

                var leftW = ( Mathf.Min( Mathf.Max( r.xMin, b.xMin ), r.xMax ) - r.xMin );
                var leftArea = sideH * leftW;
                var rightX = Mathf.Min( r.xMax, Mathf.Max( r.xMin, b.xMax ) );
                var rightW = ( r.xMax - rightX );
                var rightArea = sideH * rightW;

                return ( r.CalculateArea() - ( topArea + bottomArea + leftArea + rightArea ) );
            }

            public static Vector2 RandomPoint( this Rect r ) { return new Vector2( Random.Range( r.xMin, r.xMax ), Random.Range( r.yMin, r.yMax ) ); }
            public static Vector2 RandomPoint( this Rect r, Rect exception )
            {
                var topH = ( Mathf.Min( Mathf.Max( r.yMin, exception.yMin ), r.yMax ) - r.yMin );
                var topArea = r.width * topH;
                var bottomY = Mathf.Min( r.yMax, Mathf.Max( r.yMin, exception.yMax ) );
                var bottomH = ( r.yMax - bottomY );
                var bottomArea = r.width * bottomH;

                var sideY = Mathf.Max( r.yMin, exception.yMin );
                var sideH = Mathf.Min( r.yMax, exception.yMax ) - sideY;

                var leftW = ( Mathf.Min( Mathf.Max( r.xMin, exception.xMin ), r.xMax ) - r.xMin );
                var leftArea = sideH * leftW;
                var rightX = Mathf.Min( r.xMax, Mathf.Max( r.xMin, exception.xMax ) );
                var rightW = ( r.xMax - rightX );
                var rightArea = sideH * rightW;

                var rnd = Random.Range( 0f, topArea + bottomArea + leftArea + rightArea );

                if( rnd < topArea ) return new Vector2( Random.Range( r.xMin, r.xMax ), Random.Range( r.yMin, r.yMin + topH ) );
                rnd -= topArea;
                if( rnd < bottomArea ) return new Vector2( Random.Range( r.xMin, r.xMax ), Random.Range( bottomY, r.yMax ) );
                rnd -= bottomArea;
                if( rnd < leftArea ) return new Vector2( Random.Range( r.xMin, r.xMin + leftW ), Random.Range( sideY, sideY + sideH ) );
                rnd -= leftArea;
                if( rnd < rightArea ) return new Vector2( Random.Range( rightX, rightX + rightW ), Random.Range( sideY, sideY + sideH ) );
                return Vector2.zero;
            }
            #endregion Rect

            #region Game Object
            public static Bounds CalculateBounds( this GameObject go ) { return go.GetComponentsInChildren<Renderer>().CalculateBounds(); }
            #endregion Game Object

            #region Renderes
            public static Bounds CalculateBounds( this IEnumerable<Renderer> renderers )
            {
                var min = Vector3.one * float.MaxValue;
                var max = Vector3.one * float.MinValue;

                int count = 0;
                foreach( var r in renderers )
                {
                    count++;
                    var bounds = r.bounds;
                    min = Vector3.Min( min, bounds.min );
                    max = Vector3.Max( max, bounds.max );
                }
                if( count == 0 ) min = max = Vector3.zero;

                return K10.Utils.Unity.Algorithm.MinMaxBounds( min, max );
            }
            #endregion Renderes

            #region Colliders
            public static void Cover( this BoxCollider box, IEnumerable<Renderer> renderers ) { box.Cover( renderers.CalculateBounds() ); }
            public static void Cover( this BoxCollider box, Vector3 min, Vector3 max ) { box.Cover( K10.Utils.Unity.Algorithm.MinMaxBounds( min, max ) ); }

            public static void Cover( this BoxCollider box, Bounds bounds )
            {
                var size = bounds.size;

                if( !Mathf.Approximately( box.transform.lossyScale.x, 0 ) ) size.x /= box.transform.lossyScale.y;
                if( !Mathf.Approximately( box.transform.lossyScale.y, 0 ) ) size.y /= box.transform.lossyScale.y;
                if( !Mathf.Approximately( box.transform.lossyScale.z, 0 ) ) size.z /= box.transform.lossyScale.z;

                box.size = size;
                var center = bounds.center - box.transform.position;

                if( !Mathf.Approximately( box.transform.lossyScale.x, 0 ) ) center.x /= box.transform.lossyScale.y;
                if( !Mathf.Approximately( box.transform.lossyScale.y, 0 ) ) center.y /= box.transform.lossyScale.y;
                if( !Mathf.Approximately( box.transform.lossyScale.z, 0 ) ) center.z /= box.transform.lossyScale.z;

                box.center = center;
            }
            #endregion Colliders
        }
    }
}
