using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class DragManager
{
	Dictionary< int, DragGesture > m_currentDrags = new Dictionary<int, DragGesture>();
    SubscribersCollection<System.Func<DragGesture, bool>> m_listeners = new SubscribersCollection<System.Func<DragGesture, bool>>();
    List<int> m_toRemove = new List<int>();

	public SubscribersCollection<System.Func<DragGesture, bool>> DragEvents { get { return m_listeners; } }
	
	public bool IsNecessary { get { return m_listeners.HasSubscribers; } }
	
	public void UncheckedAll()
	{
		m_toRemove.Clear();
		m_toRemove.AddRange( m_currentDrags.Keys );
	}
	
	public List<DragGesture> VerifyUnchecked()
	{
		var ret = ( from d in m_toRemove select m_currentDrags[ d ] ).ToList();
		foreach( var key in m_toRemove )
		{
			CompleteDrag( key );
		}
		return ret;
	}
	
	public void CompleteMouseDrag() { CompleteDrag( 0 ); }
	void CompleteDrag( int dragId )
	{
		m_currentDrags[ dragId ].CompleteGesture();
		m_currentDrags.Remove( dragId );
	}
	
	void NewDragEvent( int dragId, DragGesture gesture )
	{
		m_currentDrags[ dragId ] = gesture;
		foreach( var listener in m_listeners.Subscribers )
		{
			if( listener( gesture ) )
				break;
		}
	}
	
	public void NotifyMouseDrag( Vector2 position ) { NotifyDrag( 0, position ); }
	public void NotifyDrag( int fingerId, Vector2 position )
	{
		int id = FingerIdToDragId( fingerId );
		DragGesture gest;
		if( m_currentDrags.TryGetValue( id, out gest ) )
		{
			gest.AddPoint( position );
		}
		else
		{
			NewDragEvent( id, new DragGesture( position ) );
		}
		
		m_toRemove.Remove( id );
	}
	
	int FingerIdToDragId( int fingerId ) { return fingerId + 1; }
	int DragIdToFingerId( int dragId ) { return dragId - 1;	}
	

	public class DragGesture
	{
		public event System.Action<DragGesture> OnDragComplete = (obj) => { };
		public event System.Action<DragGesture> OnDrag = (obj) => { };
		
		List<TimedPosition> m_points;
		
		public bool IsComplete { private set; get; }
		public IEnumerable<TimedPosition> Points { get { return m_points; } }
		
		public float LifeTime { get { return Last.Time - First.Time; } }
		public TimedPosition First { get { return m_points.First(); } }
		public TimedPosition Last { get { return m_points.Last(); } }
		public Vector2 FirstPoint { get { return First.Position; } }
		public Vector2 LastPoint { get { return Last.Position; } }
		
		public float Distance { get { return Vector2.Distance( LastPoint, FirstPoint ); } }
		
		public DragGesture( Vector2 beginPoint )
		{
			m_points = new List<TimedPosition>{ new TimedPosition( beginPoint ) };
		}
		
		#region Damn C#
		// Want this methods to be acessed only from CGestureInterpreter but C# Dont let me do
		public void Simplify()
		{
			m_points = new List<TimedPosition>{ First, Last };
		}
		
		internal void AddPoint( Vector2 point )
		{
			if( point != LastPoint )
			{
				m_points.Add( new TimedPosition( point ) );
				OnDrag( this );
			}
		}
		
		internal void CompleteGesture()
		{
			IsComplete = true;
			m_points.Add( new TimedPosition( LastPoint ) );
			OnDragComplete( this );
		}
		#endregion Damn C#
	}
}
