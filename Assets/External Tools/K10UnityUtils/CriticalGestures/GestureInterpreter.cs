using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class TimedPosition
{
	public TimedPosition( Vector2 pos )
	{
		Position = pos;
		Time = UnityEngine.Time.time;
	}

    public Vector2 Position { private set; get; }
	public float Time { private set; get; }
}
	
public class TapGesture
{
	public TapGesture( Vector2 pos )
	{
		Position = pos;
		Time = UnityEngine.Time.time;
	}

    public Vector2 Position { private set; get; }
	public float Time { private set; get; }
}
	
public class DoubleTapGesture : TapGesture
{
	public DoubleTapGesture( Vector2 pos, TapGesture first ) : base( pos )
	{
		FirstTap = first;
	}
	
	public TapGesture FirstTap { private set; get; }
}

public class GestureInterpreter : MonoBehaviour
{
	public float m_clickTime = .4f;
	public float m_doubleClickTime = .3f;
	public float m_clickMaxDist = 15f;
	
	private readonly DragManager m_dragManager = new DragManager();
	
	private readonly SubscribersCollection< System.Func< TapGesture, bool > > m_tapListeners = new SubscribersCollection<System.Func<TapGesture, bool>>();
//	public static event Func<bool> OnFlick;
//	public static event Func<bool> OnPinch;
//	public static event Func<bool> OnPinchComplete;
	
	private readonly List<TapGesture> m_possibleDoubleTaps = new List<TapGesture>();

	public IEnumerable<TapGesture> PossibleDoubleTaps
	{
		get
		{
			m_possibleDoubleTaps.RemoveAll( ( t ) => ( Time.time - t.Time ) > m_doubleClickTime );
			return m_possibleDoubleTaps;
		}
	}
	
	public SubscribersCollection<System.Func<TapGesture, bool>> TapEvents { get { return m_tapListeners; } }
	public SubscribersCollection<System.Func<DragManager.DragGesture, bool>> DragEvents { get { return m_dragManager.DragEvents; } }	
	
	void Update()
	{
		m_dragManager.UncheckedAll();
		
		#if UNITY_STANDALONE || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_STANDALONE_OSX || UNITY_EDITOR || UNITY_WEBPLAYER || UNITY_FLASH
		#region Mouse Interpreter
		if( Input.GetMouseButton( 0 ) )
		{
            Vector2 pos = Input.mousePosition;
            //pos.y = Screen.height - pos.y;
            m_dragManager.NotifyMouseDrag( pos );
		}
		#endregion Mouse Interpreter
		#endif
		
		foreach( var touch in Input.touches )
		{
            Vector2 pos = touch.position;
            //pos.y = Screen.height - pos.y;
            m_dragManager.NotifyDrag( touch.fingerId, pos );
		}
		
		var endedDrags = m_dragManager.VerifyUnchecked();
		
		foreach( var drag in endedDrags )
		{
			if( drag.LifeTime <= m_clickTime && drag.Distance <= m_clickMaxDist )
			{
				TapGesture firstTap = FirstTapIfImDouble( drag.LastPoint );
				TapGesture tap = null;
				
				if( firstTap != null )
				{
					tap = new DoubleTapGesture( drag.LastPoint, firstTap );
					m_possibleDoubleTaps.Remove( firstTap );
				}
				else
				{
					tap = new TapGesture( drag.LastPoint );
					m_possibleDoubleTaps.Add( tap );
				}
				
				foreach( var listener in TapEvents.Subscribers )
				{
					if( listener( tap ) )
						break;
				}
			}
		}
	}
	
	private TapGesture FirstTapIfImDouble( Vector2 position )
	{
		for( int i = m_possibleDoubleTaps.Count - 1; i >= 0; i-- )
		{
			var tap = m_possibleDoubleTaps[i];
			if( ( Time.time - tap.Time ) > m_doubleClickTime )
			{
				m_possibleDoubleTaps.RemoveAt( i );
			}
			else
			{
				if( Vector2.Distance( tap.Position, position ) <= m_clickMaxDist )
				{
					return tap;
				}
			}
		}
		return null;
	}
}
