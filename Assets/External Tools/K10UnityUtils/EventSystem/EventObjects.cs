using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public interface IEventRegister { void Register( IEventTrigger listener ); bool Unregister( IEventTrigger listener ); void Register( Action listener ); bool Unregister( Action listener ); }
public interface IEventRegister<T>: IEventRegister { void Register( IEventTrigger<T> listener ); bool Unregister( IEventTrigger<T> listener ); void Register( Action<T> listener ); bool Unregister( Action<T> listener ); }
public interface IEventRegister<T,K>: IEventRegister<T> { void Register( IEventTrigger<T,K> listener ); bool Unregister( IEventTrigger<T,K> listener ); void Register( Action<T,K>  listener ); bool Unregister( Action<T,K> listener ); }

public interface IEventTrigger { void Trigger();  bool IsValid { get; } }
public interface IEventTrigger<T> { void Trigger( T val );  bool IsValid { get; } }
public interface IEventTrigger<T,K> { void Trigger( T val, K val2 );  bool IsValid { get; } }


public class EventSlot : IEventTrigger, IEventRegister
{
	List<IEventTrigger> _listeners = new List<IEventTrigger>();
	
	public bool IsValid { get { return true; } }

    public void Trigger()
    {
        for( int i = _listeners.Count - 1; i >= 0; i-- )
        {
            var listener = _listeners[ i ];
            if( listener.IsValid ) listener.Trigger();            
            //NOT else
            if( !listener.IsValid ) _listeners.RemoveAt( i );
        }
    }
	
	public void Register( IEventTrigger listener ) { _listeners.Add( listener ); }
	public void Register( Action listener ) { _listeners.Add( new ActionEventCapsule( listener ) ); }

	public bool Unregister( IEventTrigger listener ) { return _listeners.Remove( listener ); }
	public bool Unregister( Action listener ) { return _listeners.Remove( new ActionEventCapsule( listener ) ); }
}

public class EventSlot<T> : IEventTrigger<T>, IEventRegister<T>
{
	EventSlot _generic = new EventSlot();
	List< IEventTrigger<T> > _listeners = new List< IEventTrigger<T> >();
	
	public bool IsValid { get { return true; } }
	
	public void Trigger( T t )
	{
		for( int i = _listeners.Count - 1; i >= 0; i-- )
		{
			var listener = _listeners[ i ];
			if( listener.IsValid ) listener.Trigger( t );
			//NOT else
			if( !listener.IsValid ) _listeners.RemoveAt( i );
		}
		_generic.Trigger();
	}
	
	public void Register( IEventTrigger<T> listener ) { _listeners.Add( listener ); }
	public void Register( IEventTrigger listener ) { _generic.Register( listener ); }
	public void Register( Action<T> listener ) { _listeners.Add( new ActionEventCapsule<T>( listener ) ); }
	public void Register( Action listener ) { _generic.Register( listener ); }

	public bool Unregister( IEventTrigger<T> listener ) { return _listeners.Remove( listener ); }
	public bool Unregister( IEventTrigger listener ) { return _generic.Unregister( listener ); }
	public bool Unregister( Action<T> listener ) { return _listeners.Remove( new ActionEventCapsule<T>( listener ) ); }
	public bool Unregister( Action listener ) { return _generic.Unregister( listener ); }

}

public class CventSlot<T,K> : IEventTrigger<T,K>, IEventRegister<T,K>
{
	EventSlot _generic = new EventSlot();
	EventSlot<T> _genericT = new EventSlot<T>();
	List< IEventTrigger<T,K> > _listeners = new List< IEventTrigger<T,K> >();

	public bool IsValid { get { return true; } }
	
	public void Trigger( T t, K k )
	{
		for( int i = _listeners.Count - 1; i >= 0; i-- )
		{
			var listener = _listeners[ i ];
			if( listener.IsValid ) listener.Trigger( t, k );
			//NOT else
			if( !listener.IsValid ) _listeners.RemoveAt( i );
		}
		_genericT.Trigger( t );
//		_genericK.Trigger( k );
		_generic.Trigger();
	}
	
	public void Register( IEventTrigger<T,K> listener ) { _listeners.Add( listener ); }
	public void Register( IEventTrigger<T> listener ) { _genericT.Register( listener ); }
	public void Register( IEventTrigger listener ) { _generic.Register( listener ); }
	public void Register( Action<T,K> listener ) { _listeners.Add( new ActionEventCapsule<T,K>( listener ) ); }
	public void Register( Action<T> listener ) { _genericT.Register( listener ); }
	public void Register( Action listener ) { _generic.Register( listener ); }
	
	public bool Unregister( IEventTrigger<T,K> listener ) { return _listeners.Remove( listener ); }
	public bool Unregister( IEventTrigger<T> listener ) { return _genericT.Unregister( listener ); }
	public bool Unregister( IEventTrigger listener ) { return _generic.Unregister( listener ); }
	public bool Unregister( Action<T,K> listener ) { return _listeners.Remove( new ActionEventCapsule<T,K>( listener ) ); }
	public bool Unregister( Action<T> listener ) { return _genericT.Unregister( listener ); }
	public bool Unregister( Action listener ) { return _generic.Unregister( listener ); }
}

public class CachedEventAdapter<T>: IEventTrigger
{
	IEventTrigger<T> _cache;
	T _t;
	
	public CachedEventAdapter( IEventTrigger<T> cache, T t ) { _cache = cache; _t = t; }
	public void Trigger() { _cache.Trigger( _t ); }
	bool IEventTrigger.IsValid { get { return true; } }
}

public class CachedEventAdapter<T,K>: IEventTrigger
{
	IEventTrigger<T,K> _cache;
	T _t;	K _k;
	
	public CachedEventAdapter( IEventTrigger<T,K> cache, T t, K k ) { _cache = cache; _t = t; _k = k; }
	public void Trigger() { _cache.Trigger( _t, _k ); }
	bool IEventTrigger.IsValid { get { return true; } }
}

public class BindedEvent<T>: IEventTrigger<T>
{
	IEventTrigger _binded;

	public  BindedEvent( IEventTrigger binded ) { _binded = binded; }
	public void Trigger( T val ) { _binded.Trigger(); }
    bool IEventTrigger<T>.IsValid { get { return true; } }
}

public class BindedEvent<T,K>: IEventTrigger<T,K>
{
	IEventTrigger _binded;

	public  BindedEvent( IEventTrigger binded ) { _binded = binded; }
	public void Trigger( T val, K val2 ) { _binded.Trigger(); }
    bool IEventTrigger<T,K>.IsValid { get { return true; } }
}