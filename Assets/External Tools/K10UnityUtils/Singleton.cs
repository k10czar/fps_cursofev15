using UnityEngine;
using System.Collections;

public abstract class Singleton<T> where T : UnityEngine.Object
{
    private static T m_instance;

    public static T Instance
    {
        get
        {
            if( m_instance == null )
            {
                m_instance = (T)MonoBehaviour.FindObjectOfType( typeof(T) );
				
                if( m_instance == null )
                {
//                    Debug.LogError( "An instance of " + typeof(T) + " is needed in the scene, but there is none." );
                }
            }
			
            return m_instance;
        }
    }
}

public class NamedSingletonComponent<T> where T : UnityEngine.Component
{
    private T m_instance;
    private GameObject m_cachedObject;
    private string m_name;

    public NamedSingletonComponent( string objectAttachedName )
    {
        m_name = objectAttachedName;
    }

    public T Instance
    {
        get
        {
            if( m_instance == null )
            {
                if( m_cachedObject == null )
                    m_cachedObject = GameObject.Find( m_name );

                if( m_cachedObject != null )
                    m_instance = m_cachedObject.GetComponent<T>();
            }
			
            return m_instance;
        }
    }
}

public abstract class Eternal<T> : Guaranteed<T> where T : UnityEngine.Component
{
    public new static T Instance { get { return GetInstance( true ); } }
    public new static void Request() { GetInstance( true ); }
}

public abstract class Guaranteed<T> where T : UnityEngine.Component
{    
    private static T m_instance;
    private static bool m_markedEternal = false;

    protected static T GetInstance( bool eternal )
    { 
        if( m_instance == null )
        {
            m_markedEternal = false;
            m_instance = (T)MonoBehaviour.FindObjectOfType( typeof(T) );
            
            if( m_instance == null )
            {
                GameObject obj = new GameObject( string.Format( "_GS_{0}", ( typeof(T) ).ToString() ) );
                m_instance = obj.AddComponent<T>();
            }
        }

        if( eternal && !m_markedEternal )
        {
            GameObject.DontDestroyOnLoad( m_instance.transform );
            m_markedEternal = true;
            m_instance.name = string.Format( "_ES_{0}", ( typeof(T) ).ToString() );
        }
        
        return m_instance; 
    }

    public static T Instance { get { return GetInstance( false ); } }
    public static void Request() { GetInstance( false ); }
}
