﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class RandomClipFX
{
	[SerializeField]List<AudioClip> _clips;
	[SerializeField]AudioSource _source;
	[SerializeField]float _delay = 0f;
	[SerializeField]float _volume = 1f;
	[SerializeField]float _pitch = 1f;
	
	public void PlaySomeClip( Vector3 pos )
	{
        if( _clips.Count == 0 || Mathf.Approximately( _volume, 0 ) )
			return;

        var clip = _clips[ Random.Range( 0, _clips.Count ) ];
        Guaranteed<AudioManager>.Instance.Play( clip, pos, _volume, _delay, _pitch, 0 );
	}
}