﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour
{
    public bool Play( AudioClip clip, float volume )
    {
        return Play( clip, Vector3.zero, volume, 0, 1, 0 );
    }

    public bool Play( AudioClip clip, Vector3 position, float volume, float delay, float pitch, float pan )
    {
        if( clip == null )
        {
            Debug.LogError( "Cannot play sound clip, because it is null" );
            return false;
        }
        
        var go = new GameObject( clip.name );
        go.transform.position = position;
        go.transform.parent = transform;
        var source = go.AddComponent<AudioSource>();
        source.volume = volume;
        source.clip = clip;
        source.pitch = pitch;
        source.pan = pan;
        source.PlayDelayed( delay );
        var dd = go.AddComponent<DelayedDestroy>();
        dd._life = delay + clip.length + 0.1f;
        return true;
    }
}