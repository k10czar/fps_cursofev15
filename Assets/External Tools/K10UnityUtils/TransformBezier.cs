﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TransformBezier : MonoBehaviour
{
    [SerializeField] List<Transform> _points = new List<Transform>();
    List<Vector3> _tempPts = new List<Vector3>();
    
    List<Vector3> Points
    {
        get
        {
            List<Vector3> pts = new List<Vector3>();
            foreach( var t in _points ) { if( t!= null ) pts.Add( t.position ); }
            return pts;
        }
    }
    
    
    public Vector3 GetPoint( float percentage )
    {
        percentage = Mathf.Clamp01( percentage );

        for( int i = 0; i < _points.Count; i++ )
        {
            var t = _points[ i ];
            if( _tempPts.Count > i ) _tempPts[ i ] = t.position;
            else _tempPts.Add( t.position );
        }

        int it = _tempPts.Count - 1;
        while( it > 0 )
        {
            for( int i = 0; i < it; i++ )
            {
                _tempPts[ i ] = ( ( _tempPts[ i ] * ( 1f - percentage ) ) + ( _tempPts[ i + 1 ] * percentage ) );
            }
            it--;
        }

        return ( _tempPts.Count == 0 ) ? Vector3.zero : _tempPts[ 0 ];
    }
    
    
    void OnDrawGizmos()
    {
        int interactions = 30;
        
        float step = 1f / interactions;
        
        var pts = Points;
        for( int i = 1; i < _points.Count; i++ )
        {
            Debug.DrawLine( _points[ i - 1 ].position, _points[ i ].position, Color.red );
        }
        
        for( int i = 0; i <= interactions; i++ )
        {
            float pc = i * step;
            Debug.DrawLine( GetPoint( pc ), GetPoint( pc + step ), Color.yellow );
        }
    }
}
