﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;

[ExecuteInEditMode]
#endif
public class FaceCamera : MonoBehaviour
{
	public bool m_cameraRotation;

	void LateUpdate()
	{
		var cam = Camera.main;
		var rotation = m_cameraRotation ? cam.transform.rotation * Vector3.up : Vector3.up;
		transform.LookAt( transform.position + cam.transform.rotation * Vector3.forward, rotation );
	}
}