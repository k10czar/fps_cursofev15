﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CameraShakeInfo
{
    [SerializeField] float _shakeTime;
    [SerializeField] float _shakeAmplitude;
    [SerializeField] float _shakeFrequency;
    [SerializeField] float _decayPower;

    public float Time { get { return _shakeTime; } }
    public float Amplitude { get { return _shakeAmplitude; } }
    public float Frequency { get { return _shakeFrequency; } }
    public float DecayPower { get { return _decayPower; } }

    public CameraShakeInfo() { }

    public CameraShakeInfo( float amplitude, float frequency, float time )
        : this( amplitude, frequency, time, 1 )
    { }

    public CameraShakeInfo( float amplitude, float frequency, float time, float decayPower ) 
    {
        _shakeTime = time;
        _shakeAmplitude = amplitude;
        _shakeFrequency = frequency;
        _decayPower = decayPower;
    }
}

public class CameraShake : MonoBehaviour
{
    [SerializeField] CameraShakeInfo m_test;

    float _shakeTime;
    float _shakeAmplitude;
    float _shakeFrequency;
    float _accTime;
    float _decayPower;
    float _randomStart;

    void AddCameraShake( CameraShakeInfo info )
    {
        if( _accTime < _shakeTime )
        {
            _shakeTime = Mathf.Max( _shakeTime - _accTime, info.Time );
            _shakeAmplitude = Mathf.Max( _shakeAmplitude, info.Amplitude );
            _shakeFrequency = Mathf.Max( _shakeFrequency, info.Frequency );
            _decayPower = info.DecayPower;
        }
        else
        {
            _shakeTime = info.Time;
            _shakeAmplitude = info.Amplitude;
            _shakeFrequency = info.Frequency;
            _decayPower = info.DecayPower;
        }
        _accTime = 0;
        _randomStart = Random.Range( 0f, 10f );
    }

    float CurrentAmplitude { get { return _shakeAmplitude * Mathf.Pow( ( _shakeTime - _accTime ) / _shakeTime, _decayPower ); } }

    public static void Add( CameraShakeInfo info )
    {
        Camera cam = Camera.main;

        var shake = cam.GetComponent<CameraShake>();
        if( shake == null ) shake = cam.gameObject.AddComponent<CameraShake>();

        shake.AddCameraShake( info );
    }

	void Update() 
    {
        if( Input.GetKeyDown( KeyCode.O ) )
        {
            AddCameraShake( m_test );
        }

        if( _accTime < _shakeTime )
        {
            _accTime += Time.deltaTime;
            if( _accTime > _shakeTime )
            {
                _accTime = _shakeTime;
            }

            float amplitude = Mathf.PerlinNoise( _randomStart + _shakeFrequency * _accTime, 0 ) * CurrentAmplitude;
            float angle = Mathf.PerlinNoise( 0, _randomStart + _shakeFrequency * _accTime ) * Mathf.PI * 2;

            transform.localPosition = ( Vector3.right * Mathf.Cos( angle ) + Vector3.up * Mathf.Sin( angle ) ) * amplitude;
        }
        else
        {
            transform.localPosition = Vector3.zero;
        }
	}
}
