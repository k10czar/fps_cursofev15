using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class RendererSortingSetter : AttachedComponent<Renderer>
{
    public string m_sortingLayer = "";
    string m_lastSettedSortingLayer = "";

    public int m_sortingOrder = 0;
    int m_lastSettedSortingOrder = 0;    

    void Awake()
    {
        Update();
    }

	void Update()
    {
        if( m_lastSettedSortingOrder != m_sortingOrder )
        {
            Attached.sortingOrder = m_sortingOrder;
            m_lastSettedSortingOrder = m_sortingOrder;
        }

        if( m_lastSettedSortingLayer != m_sortingLayer )
        {
            Attached.sortingLayerName = m_sortingLayer;
            m_lastSettedSortingLayer = m_sortingLayer;
        }
	}
}
