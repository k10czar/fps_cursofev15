﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using K10.Extentions;
using K10.Utils.Unity;

public class EditorUtils : EditorWindow
{
    [ MenuItem( "K10/BakeScale" ) ]
    static void BakeScale()
    {
        var selection = Selection.gameObjects;
        if( selection == null || selection.Length == 0 )
        {
            Debug.Log( "There are no slection to bake" );
            return;
        }
        
        foreach( var selected in selection )
        {
            Algorithm.BakeScale( selected.transform );
        }
    }

    [MenuItem( "K10/AttachBoxCollider" )]
    static void AttachBoxCollider()
    {
        var selection = Selection.gameObjects;
        if( selection == null || selection.Length == 0 )
        {
            Debug.Log( "There are no slection to bake" );
            return;
        }

        foreach( var selected in selection )
        {
            Undo.RecordObject( selected, "Attach Box Collider" );
            var boxCollider = selected.GetComponent<BoxCollider>();
            if( boxCollider == null ) boxCollider = selected.AddComponent<BoxCollider>();

            boxCollider.Cover( selected.GetComponentsInChildren<Renderer>() );
        }
    }

    [MenuItem( "K10/InflateBoxCollider" )]
    static void InflateBoxCollider()
    {
        var selection = Selection.gameObjects;
        if( selection == null || selection.Length == 0 )
        {
            Debug.Log( "There are no slection to bake" );
            return;
        }

        foreach( var selected in selection )
        {
            Undo.RecordObject( selected, "Inflate Box Collider" );
            var boxCollider = selected.GetComponent<BoxCollider>();
            if( boxCollider == null )
                continue;

            boxCollider.size = boxCollider.size * 1.1f;
        }
    }

    [MenuItem( "K10/DeflateBoxCollider" )]
    static void DeflateBoxCollider()
    {
        var selection = Selection.gameObjects;
        if( selection == null || selection.Length == 0 )
        {
            Debug.Log( "There are no slection to bake" );
            return;
        }

        foreach( var selected in selection )
        {
            Undo.RecordObject( selected, "Deflate Box Collider" );
            var boxCollider = selected.GetComponent<BoxCollider>();
            if( boxCollider == null )
                continue;

            boxCollider.size = boxCollider.size * ( 1 / 1.1f );
        }
    }

    [MenuItem( "K10/AdvanceLayer" )]
    static void AdvanceLayer()
    {
        var selection = Selection.gameObjects;
        if( selection == null || selection.Length == 0 )
        {
            Debug.Log( "There are no slection to bake" );
            return;
        }

        foreach( var selected in selection )
        {
            foreach( var rend in selected.GetComponentsInChildren<Renderer>() ) { rend.sortingLayerID++; }
           // foreach( var rend in selected.GetComponentsInChildren<RendererSortingSetter>() ) { rend.sortingLayerID++; }
        }
    }

    [MenuItem( "K10/SubtractLayer" )]
    static void SubtractLayer()
    {
        var selection = Selection.gameObjects;
        if( selection == null || selection.Length == 0 )
        {
            Debug.Log( "There are no slection to bake" );
            return;
        }

        foreach( var selected in selection )
        {
            foreach( var rend in selected.GetComponentsInChildren<Renderer>() ) { rend.sortingLayerID--; }
            //foreach( var rend in selected.GetComponentsInChildren<RendererSortingSetter>() ) { rend.sortingLayerID--; }
        }
    }
    
    [ MenuItem( "K10/AdvanceOrder %+" ) ]
    static void AdvanceOrder()
    {
        var selection = Selection.gameObjects;
        if( selection == null || selection.Length == 0 )
        {
            Debug.Log( "There are no slection to bake" );
            return;
        }
        
        foreach( var selected in selection )
        {
            foreach( var rend in selected.GetComponentsInChildren<Renderer>() ) { rend.sortingOrder++; }
            foreach( var rend in selected.GetComponentsInChildren<RendererSortingSetter>() ) { rend.m_sortingOrder++; }
        }
    }
    
    [ MenuItem( "K10/SubtractOrder %-" ) ]
    static void SubtractOrder()
    {
        var selection = Selection.gameObjects;
        if( selection == null || selection.Length == 0 )
        {
            Debug.Log( "There are no slection to bake" );
            return;
        }
        
        foreach( var selected in selection )
        {
            foreach( var rend in selected.GetComponentsInChildren<Renderer>() ) { rend.sortingOrder--; }
            foreach( var rend in selected.GetComponentsInChildren<RendererSortingSetter>() ) { rend.m_sortingOrder--; }
        }
    }
}