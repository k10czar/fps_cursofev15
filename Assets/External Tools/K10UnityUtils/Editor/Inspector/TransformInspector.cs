﻿// Alternative version, with redundant code removed
using UnityEngine;
using UnityEditor;
using System.Collections;

[CanEditMultipleObjects]
[CustomEditor(typeof(Transform))]
public class TransformInspector : Editor
{
	static bool _positionAdvanced;
	static Vector3 _minPos = Vector3.zero;
	static Vector3 _maxPos = Vector3.one;
	
	static bool _rotationAdvanced;
	static Vector3 _minRot = Vector3.one * -180;
	static Vector3 _maxRot = Vector3.one * 180;
	
	static bool _scaleAdvanced;
	static bool _sameRangeValue;
	static Vector3 _minScl = Vector3.zero;
	static Vector3 _maxScl = Vector3.one;
	static float _minBaseScl = 0;
	static float _maxBaseScl = 1;

	public override void OnInspectorGUI()
	{
//		foreach( var target in targets )
//		{
		Transform t = (Transform)target;
		
		// Replicate the standard transform inspector gui
		EditorGUIUtility.LookLikeControls();
		EditorGUI.indentLevel = 0;
		
		Vector3 initialposition = t.localPosition;
		Vector3 initialeulerAngles = t.localEulerAngles;
		Vector3 initialscale = t.localScale;

		Vector3 position = t.localPosition;
		Vector3 eulerAngles = t.localEulerAngles;
		Vector3 scale = t.localScale;

		GUILayoutOption[] opt = new GUILayoutOption[]{ /*GUILayout.MaxWidth(80.0f),*/ GUILayout.MaxHeight(20.0f) };

		if( targets.Length > 1 )
			GUI.color = Color.Lerp( Color.yellow, Color.white, .8f );

		EditorGUILayout.BeginHorizontal( opt );
		bool resetPos = GUILayout.Button( "P", GUILayout.Width( 20f ) );
		position = EditorGUILayout.Vector3Field( "", position );
		_positionAdvanced = GUILayout.Toggle( _positionAdvanced, "", GUILayout.Width( 20f ) );
		EditorGUILayout.EndHorizontal();

		var color = GUI.color;
		GUI.color = Color.Lerp( Color.blue, color, .8f );
		bool rndPX = false, rndPY = false, rndPZ = false;
		if( _positionAdvanced )
		{
			EditorGUILayout.BeginHorizontal( opt );
			GUILayout.Label( "Randomize", GUILayout.Width( 70f ) );
			rndPX = GUILayout.Button( "X?", GUILayout.Width( 25f ) );
			rndPY = GUILayout.Button( "Y?", GUILayout.Width( 25f ) );
			rndPZ = GUILayout.Button( "Z?", GUILayout.Width( 25f ) );
			var all = GUILayout.Button( "All?", GUILayout.Width( 35f ) );

			if( all ) 
				rndPX = rndPY = rndPZ = true;

			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal( opt );
			GUILayout.Label( "min", GUILayout.Width( 30f ) );
			_minPos = EditorGUILayout.Vector3Field( "", _minPos );
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal( opt );
			GUILayout.Label( "max", GUILayout.Width( 30f ) );
			_maxPos = EditorGUILayout.Vector3Field( "", _maxPos );
			//			EditorGUILayout.EndHorizontal();
			EditorGUILayout.EndHorizontal();
		}
		GUI.color = color;
		
		EditorGUILayout.BeginHorizontal( opt );
		bool resetRot = GUILayout.Button( "R", GUILayout.Width( 20f ) );
		eulerAngles = EditorGUILayout.Vector3Field( "", eulerAngles );
		_rotationAdvanced = GUILayout.Toggle( _rotationAdvanced, "", GUILayout.Width( 20f ) );
		EditorGUILayout.EndHorizontal();

		color = GUI.color;
		GUI.color = Color.Lerp( Color.blue, color, .8f );
		bool rndRX = false, rndRY = false, rndRZ = false;
		if( _rotationAdvanced )
		{
			EditorGUILayout.BeginHorizontal( opt );
			GUILayout.Label( "Randomize", GUILayout.Width( 70f ) );
			rndRX = GUILayout.Button( "X?", GUILayout.Width( 25f ) );
			rndRY = GUILayout.Button( "Y?", GUILayout.Width( 25f ) );
			rndRZ = GUILayout.Button( "Z?", GUILayout.Width( 25f ) );
			var all = GUILayout.Button( "All?", GUILayout.Width( 35f ) );
			
			if( all ) 
				rndRX = rndRY = rndRZ = true;
			
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal( opt );
			GUILayout.Label( "min", GUILayout.Width( 30f ) );
			_minRot = EditorGUILayout.Vector3Field( "", _minRot );
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal( opt );
			GUILayout.Label( "max", GUILayout.Width( 30f ) );
			_maxRot = EditorGUILayout.Vector3Field( "", _maxRot );
			EditorGUILayout.EndHorizontal();
		}
		GUI.color = color;
		
		EditorGUILayout.BeginHorizontal( opt );
		bool resetScl = GUILayout.Button( "R", GUILayout.Width( 20f ) );
		scale = EditorGUILayout.Vector3Field( "", scale );
		_scaleAdvanced = GUILayout.Toggle( _scaleAdvanced, "", GUILayout.Width( 20f ) );
		EditorGUILayout.EndHorizontal();
		
		color = GUI.color;
		GUI.color = Color.Lerp( Color.blue, color, .8f );
		bool rndSX = false, rndSY = false, rndSZ = false, bakeScl = false;
		if( _scaleAdvanced )
		{
			EditorGUILayout.BeginHorizontal( opt );
			bakeScl = GUILayout.Button( "Bake", GUILayout.Width( 45f ) );
			GUILayout.Space( 45f );
			GUILayout.Label( "Randomize", GUILayout.Width( 70f ) );
			rndSX = GUILayout.Button( "X?", GUILayout.Width( 25f ) );
			rndSY = GUILayout.Button( "Y?", GUILayout.Width( 25f ) );
			rndSZ = GUILayout.Button( "Z?", GUILayout.Width( 25f ) );
			var all = GUILayout.Button( "All?", GUILayout.Width( 35f ) );
			if( all )
				rndSX = rndSY = rndSZ = true;

			_sameRangeValue = GUILayout.Toggle( _sameRangeValue, "Same", GUILayout.Width( 50f ) );
			EditorGUILayout.EndHorizontal();

			if( _sameRangeValue )
			{
				_minBaseScl = EditorGUILayout.FloatField( "min", _minBaseScl );
				_maxBaseScl = EditorGUILayout.FloatField( "max", _maxBaseScl );
			}
			else
			{
				EditorGUILayout.BeginHorizontal( opt );
				GUILayout.Label( "min", GUILayout.Width( 30f ) );
				_minScl = EditorGUILayout.Vector3Field( "", _minScl );
				EditorGUILayout.EndHorizontal();
				EditorGUILayout.BeginHorizontal( opt );
				GUILayout.Label( "max", GUILayout.Width( 30f ) );
				_maxScl = EditorGUILayout.Vector3Field( "", _maxScl );
				EditorGUILayout.EndHorizontal();
			}
		}
		GUI.color = color;

		EditorGUIUtility.LookLikeInspector();
		
		if( GUI.changed )
		{

			foreach( var currentTarget in targets )
			{
				var current = (Transform)currentTarget;
				Undo.RegisterUndo( current, "Transform Change" );
				
				var localPosition = current.localPosition;
				if( initialposition.x != position.x ) localPosition.x = position.x;
				if( initialposition.y != position.y ) localPosition.y = position.y;
				if( initialposition.z != position.z ) localPosition.z = position.z;

				var localEulerAngles = current.localEulerAngles;
				if( initialeulerAngles.x != eulerAngles.x ) localEulerAngles.x = eulerAngles.x;
				if( initialeulerAngles.y != eulerAngles.y ) localEulerAngles.y = eulerAngles.y;
				if( initialeulerAngles.z != eulerAngles.z ) localEulerAngles.z = eulerAngles.z;

				var localScale = current.localScale;
				if( initialscale.x != scale.x ) localScale.x = scale.x;
				if( initialscale.y != scale.y ) localScale.y = scale.y;
				if( initialscale.z != scale.z ) localScale.z = scale.z;

				if( resetPos ) localPosition = Vector3.zero;
				if( rndPX ) localPosition.x = Random.Range( _minPos.x, _maxPos.x );
				if( rndPY ) localPosition.y = Random.Range( _minPos.y, _maxPos.y );
				if( rndPZ ) localPosition.z = Random.Range( _minPos.z, _maxPos.z );

				if( resetRot ) localEulerAngles = Vector3.zero;
				if( rndRX ) localEulerAngles.x = Random.Range( _minRot.x, _maxRot.x );
				if( rndRY ) localEulerAngles.y = Random.Range( _minRot.y, _maxRot.y );
				if( rndRZ ) localEulerAngles.z = Random.Range( _minRot.z, _maxRot.z );

				var baseScale = Random.Range( _minBaseScl, _maxBaseScl );
				if( resetScl ) localScale = Vector3.one;
				if( rndSX ) localScale.x = _sameRangeValue ? baseScale : Random.Range( _minScl.x, _maxScl.x );
				if( rndSY ) localScale.y = _sameRangeValue ? baseScale : Random.Range( _minScl.y, _maxScl.y );
				if( rndSZ ) localScale.z = _sameRangeValue ? baseScale : Random.Range( _minScl.z, _maxScl.z );

				current.localPosition = FixIfNaN( localPosition );
				current.localEulerAngles = FixIfNaN( localEulerAngles );
				current.localScale = FixIfNaN( localScale );

				if( bakeScl ) K10.Utils.Unity.Algorithm.BakeScale( t );
			}
		}
	}
	
	private Vector3 FixIfNaN(Vector3 v)
	{
		if (float.IsNaN(v.x))
		{
			v.x = 0;
		}
		if (float.IsNaN(v.y))
		{
			v.y = 0;
		}
		if (float.IsNaN(v.z))
		{
			v.z = 0;
		}
		return v;
	}
	
}