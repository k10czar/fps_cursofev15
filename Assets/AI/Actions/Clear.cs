using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class Clear : RAINAction
{
	public string _variable = "playerTarget";

    public override void Start(RAIN.Core.AI ai)
    {
		ai.WorkingMemory.SetItem<GameObject>( _variable, null );
		
        base.Start(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}