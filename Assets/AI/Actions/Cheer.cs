using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class Cheer : RAINAction
{
	float time;

    public override void Start( RAIN.Core.AI ai )
    {
		var anim = ai.Body.GetComponentInChildren<Animator>();
		anim.SetTrigger( "Top" );

		time = Time.time;

        base.Start( ai );
    }

    public override ActionResult Execute( RAIN.Core.AI ai )
    {
		if( Time.time - time < 1 )
			return ActionResult.RUNNING;

        return ActionResult.SUCCESS;
    }

    public override void Stop( RAIN.Core.AI ai )
    {
        base.Stop( ai );
    }
}