using UnityEngine;
using System.Collections;

public class RainInfoLoader : MonoBehaviour 
{
	const float K_BASE_ANIM_SPPED = 4;

	[SerializeField] CharacterController _charController;
	[SerializeField] Animator _animator;
	
	[SerializeField] FloatAnimator _vx;
	[SerializeField] FloatAnimator _vy;

	void Start() 
	{
		_charController = GetComponentInParent<CharacterController>();
		_animator = GetComponent<Animator>();

		var vel = _charController.velocity;
		_vx.Start( Vector3.Dot( vel, transform.right ) / K_BASE_ANIM_SPPED );
		_vy.Start( Vector3.Dot( vel, transform.forward ) / K_BASE_ANIM_SPPED );
	}


	void Update() 
	{

		var vel = _charController.velocity;
		
		//		Debug.Log( Vector3.Dot( vel, transform.forward ) );
		_vx.SetDesire( Vector3.Dot( vel, transform.right ) / K_BASE_ANIM_SPPED );
		_vy.SetDesire( Vector3.Dot( vel, transform.forward ) / K_BASE_ANIM_SPPED );
		
		_vx.Update (Time.deltaTime);
		_vy.Update (Time.deltaTime);

//		;
		_animator.SetFloat( "VX", _vx.Value );
		_animator.SetFloat( "VZ", _vy.Value );
	}
}
