﻿using UnityEngine;
using System.Collections;

public class HitPart : MonoBehaviour 
{
	//public DummyEnemy Dummy { get; private set; }

	System.Action<Transform> _hit;

	public void Register( System.Action<Transform> registerHeadHit/*, DummyEnemy dummy*/ )
	{
		_hit = registerHeadHit;
		//Dummy = dummy;
	}

	public void Hit( Transform origin )
	{
		_hit( origin );
	}
}
