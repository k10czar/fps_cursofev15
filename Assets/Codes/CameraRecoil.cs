﻿using UnityEngine;
using System.Collections;

public class CameraRecoil : MonoBehaviour
{
	[System.Serializable]
	public class MovementPhysics
	{
		public float _gravity = -400f;
		public float _maxValue = 2;
		float _velocity = 0;
		float _value = 0;

		public float Velocity { set { _velocity = value;  } }
		public float Current { get { return _value; } }

		public void Update( float delta )
		{
			_value += _velocity * delta;
			_velocity += _gravity * delta;
			_value = Mathf.Clamp( _value, 0, _maxValue );
		}
	}
	
	public MovementPhysics _rotationXModifier;

	void Update()
	{
		_rotationXModifier.Update( Time.deltaTime );
		transform.localRotation = Quaternion.Euler( -_rotationXModifier.Current, 0, 0 );
	}

	public void Shake( float power )
	{
		_rotationXModifier.Velocity = power;
	}
}
