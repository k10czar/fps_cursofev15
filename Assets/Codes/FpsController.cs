using UnityEngine;
using System.Collections;

public class FpsController : AttachedComponent<CharacterMovementController>
{
	public Vector2 m_sensitivity = Vector2.one * 5;
	public static float m_sensitivityModifier = 1;
	public float minimumY = -80;
	public float maximumY = 80;
	
	public Transform _horizontalRotation;
	public Transform _verticalRotation;
	
	float rotationY = 0;

	void Start()
	{
//		Screen.showCursor = false;
//		Screen.lockCursor = true;
	}
	
	public void Stop()
	{
		enabled = false;
	}
	
	void FixedUpdate()
	{
		Screen.showCursor = false;
		Screen.lockCursor = true;

		float rotationX = _horizontalRotation.localEulerAngles.y + Input.GetAxis( "Mouse X" ) * m_sensitivity.x * m_sensitivityModifier;
		
		rotationY += Input.GetAxis( "Mouse Y" ) * m_sensitivity.y * m_sensitivityModifier;
		rotationY = Mathf.Clamp( rotationY, minimumY, maximumY );
		
		_horizontalRotation.localEulerAngles = new Vector3( 0, rotationX, 0 );
		_verticalRotation.localEulerAngles = new Vector3( -rotationY, 0, 0 );
		
		var directionVector = new Vector3( Input.GetAxis( "Horizontal" ), 0, Input.GetAxis( "Vertical" ) );
		
		if( directionVector != Vector3.zero )
		{
			var directionLength = directionVector.magnitude;
			directionVector = directionVector / directionLength;
			
			directionLength = Mathf.Min( 1, directionLength );
			
			directionLength = directionLength * directionLength;
			directionVector = directionVector * directionLength;
		}
		
		Attached.UpdateMovement( directionVector, Input.GetButton( "Jump" ), Time.deltaTime );
	}
}

