using UnityEngine;
using System.Collections;


public class CharacterMovementController : AttachedComponent<CharacterController>
{
	Vector3 m_currentVelocity = Vector3.zero;
	Vector3 m_kickVelocityMod = Vector3.zero;
	
	public float m_maxSpeed = 9f;
	public float m_acceleration = 120f;
	public float m_friction = 80f;
	public float m_stopThreshold = 1.5f;
	public float m_gravity = 23f;
	public float m_jumpSpeed = 10f;
	public float m_kickSpeed = 20f;
	public float m_kickStopThreshold = 1.5f;
	public float m_kickfriction = 20f;
	
	public RandomClipFX m_jumpSound;
	public RandomClipFX m_footstepSound;

	int Vines { get; set; }
	
	float m_velY = 0;
	
	bool m_lastGround;

	public RandomFloatRange m_footstepInterval = new RandomFloatRange( .25f, .3f );
	float m_nextStep = 0;
	float m_accTime = 0;
	float m_lastThrust = 0;

	public Animator m_movementAnimator;

	void Awake()
	{
		ResetStep();
	}

	public void Stop()
	{
		if( m_movementAnimator )
			m_movementAnimator.SetFloat( "Velocity", 0 );
		enabled = false;
	}

	public void ResetStep()
	{
		m_nextStep = m_footstepInterval.Random;
		m_accTime = 0;
	}

	public void UpdateAnimator()
	{
		if( m_movementAnimator == null )
			return;

		AnimatorStateInfo stateInfo = m_movementAnimator.GetCurrentAnimatorStateInfo( 0 );
		var hash = stateInfo.nameHash;
		if( hash == Animator.StringToHash( "Base Layer.HandJump" ) )
		{
			m_movementAnimator.SetBool( "Handout", false );
		}
	}

	public void KickThrust()
	{
		if( m_movementAnimator )
		m_movementAnimator.SetBool( "Handout", true );
		m_kickVelocityMod += Attached.transform.forward * m_kickSpeed;
		m_lastThrust = Time.time;
		ResetStep();
	}

	void Playfootstep()
	{
		ResetStep();
		if( m_kickVelocityMod.magnitude <= m_kickStopThreshold /*Time.time - m_lastThrust < m_kickSpeed / m_kickfriction*/ )
		{
			m_footstepSound.PlaySomeClip( transform.position );
		}
	}
	
	public void UpdateMovement( Vector3 inputMoveDirection, bool jump, float delta )
	{
		inputMoveDirection = Attached.transform.rotation * inputMoveDirection * m_acceleration;

		if( m_kickVelocityMod.magnitude > m_kickStopThreshold )
		{
			float val = Mathf.Min( m_kickfriction * delta, m_kickVelocityMod.magnitude );
			m_kickVelocityMod -= m_kickVelocityMod.normalized * val;
		}
		else
		{
			m_kickVelocityMod = Vector3.zero;
		}

		if( IsGrounded )
		{
			if( m_currentVelocity.magnitude / m_maxSpeed > .1f )
			{
				m_accTime += Time.deltaTime;
				if( m_accTime >= m_nextStep )
				{
					Playfootstep();
				}
			}

			m_currentVelocity.y = 0;
			
			m_currentVelocity += inputMoveDirection * delta;
			
			if( m_currentVelocity.magnitude > m_stopThreshold )
			{
				float val = Mathf.Min( m_friction * delta, m_currentVelocity.magnitude );
				m_currentVelocity -= m_currentVelocity.normalized * val;
			}
			else
			{
				m_currentVelocity = Vector3.zero;
			}
			
			var mag = m_currentVelocity.magnitude;
			if( mag > m_maxSpeed )
			{
				m_currentVelocity *= m_maxSpeed / mag;
			}
			
			m_currentVelocity.y = 0;

			if( m_movementAnimator )
				m_movementAnimator.SetFloat( "Speed", m_currentVelocity.magnitude / m_maxSpeed );
		}
		else
		{
			m_currentVelocity.y -= m_gravity * delta;
		}
		
		if( jump && IsGrounded )
		{
			if( m_movementAnimator )
			m_movementAnimator.SetBool( "Handout", true );
			m_jumpSound.PlaySomeClip( transform.position );
			m_currentVelocity.y = m_jumpSpeed;
			ResetStep();
		}
		
		if( m_currentVelocity.magnitude > .1f || m_kickVelocityMod.magnitude > .1f )
		{
			var step = ( m_currentVelocity + m_kickVelocityMod ) * delta;
			if( Mathf.Approximately( step.y, 0 ) )
				step.y = 0;
			Attached.Move( step );
		}
		
		m_lastGround = Attached.isGrounded;
	}

	void SpeedReset()
	{
		m_currentVelocity = Vector3.zero;
		m_kickVelocityMod = Vector3.zero;
		if( m_movementAnimator )
		m_movementAnimator.SetFloat( "Velocity", 0 );
	}
	
	bool IsGrounded { get { return m_lastGround || Attached.isGrounded; } }
}