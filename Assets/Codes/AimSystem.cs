﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class AimSystem : MonoBehaviour
{
	public RandomClipFX _shotSound;

	[SerializeField]float _force = 250f;
	[SerializeField]CameraRecoil _shake;
	[SerializeField]float _shotShakePower = 20f;
	[SerializeField]GameObject _impactFX;
	[SerializeField]Animator _animator;
	[SerializeField]GameObject _shootFX;
	[SerializeField]Transform _shootFXpos;

	public Ray Direction { get { return new Ray( transform.position, transform.forward ); } }
	
	public void Fire()
	{
		if( _animator != null )
		{
			_animator.SetTrigger( "Shot" );
		}

		if( _shootFX != null )
		{
//			Debug.Log( "shoot" );
			GameObject.Instantiate( _shootFX, _shootFXpos.position, _shootFXpos.rotation );
		}

		RaycastHit hitInfo;
		bool hitted = Physics.Raycast( Direction, out hitInfo/*, ( 1 << LayerManager.Floor ) + ( 1 << LayerManager.Enemies ) + ( 1 << LayerManager.Obstacles )*/ );
		float range = ( hitted ) ? hitInfo.distance : Mathf.Infinity;

		_shotSound.PlaySomeClip( transform.position );
		_shake.Shake( _shotShakePower );

//		Debug.Log( ( hitted ) ? ( "Hitted " + hitInfo.transform.name + " on layer " + LayerMask.LayerToName( hitInfo.transform.gameObject.layer ) ) : "Not hitted" );

		if( hitted )
		{
			var dir = ( hitInfo.point - transform.position ).normalized;
			var hitPart = hitInfo.collider.GetComponent<HitPart>();
			if( hitPart )
			{
				hitPart.Hit( transform );
			}

			//				Guaranteed<FxManager>.Instance.Play( _dirtyParticle, hitInfo.point, Quaternion.LookRotation( hitInfo.normal ) );

			var rigidBody = hitInfo.collider.GetComponentInChildren<Rigidbody>();
			if( rigidBody )
			{
				rigidBody.AddForce( _force * dir );
			}

//			if( _impactFX != null ) 
//				GameObject.Instantiate( _impactFX, hitInfo.point - dir * .2f, Quaternion.LookRotation( dir ) );

			GameObject.Instantiate( _impactFX, hitInfo.point, Quaternion.LookRotation( dir ) );
		}
	}
}
