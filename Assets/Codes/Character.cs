﻿using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour
{
	[SerializeField] AimSystem _aim;

	void Start() 
	{
		_aim = GetComponentInChildren<AimSystem>();
	}


	void Update() 
	{		
		if( Input.GetButtonDown( "Fire1" ) ) Shot();		
		if( Input.GetButtonDown( "Fire2" ) ) Alternate();
	}

	void Shot()
	{
		_aim.Fire();
	}

	void Alternate()
	{

	}
}
