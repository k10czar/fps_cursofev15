﻿using UnityEngine;
using System.Collections;

public class PushTest : MonoBehaviour 
{
	[SerializeField] float _force = 10;
	[SerializeField] float _radius = 1;


	void Update() 
	{
		if( Input.GetKeyDown( KeyCode.E ) )
		{
			var cols = Physics.OverlapSphere( transform.position, _radius );

			foreach( var col in cols )
			{
				if( col.rigidbody != null )
				{
					col.rigidbody.AddExplosionForce( _force, transform.position, _radius );
				}
			}
		}
	}
}
