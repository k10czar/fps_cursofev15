﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class AutoRotate : MonoBehaviour 
{
	[SerializeField] float _speed;


	void Update() 
	{
		 transform.rotation = Quaternion.Euler( 0, _speed * Time.time, 0 );
	}
}
